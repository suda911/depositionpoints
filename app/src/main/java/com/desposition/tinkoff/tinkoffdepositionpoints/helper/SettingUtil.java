package com.desposition.tinkoff.tinkoffdepositionpoints.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

final class SettingUtil {

    @Nullable
    static String getString(@NonNull Context context,
                            @StringRes int keyId,
                            @Nullable String defaultValue) {
        String key = context.getString(keyId);
        return getPreferences(context).getString(key, defaultValue);
    }

    @Nullable
    static String getString(@NonNull Context context, @Nullable String key, @Nullable String defaultValue) {
        return getPreferences(context).getString(key, defaultValue);
    }

    static int getInteger(@NonNull Context context, @StringRes int keyId, int defaultValue) {
        String key = context.getString(keyId);
        return getPreferences(context).getInt(key, defaultValue);
    }

    static boolean getBoolean(@NonNull Context context,
                              @StringRes int keyId,
                              boolean defaultValue) {
        String key = context.getString(keyId);
        return getPreferences(context).getBoolean(key, defaultValue);
    }

    static void putBoolean(@NonNull Context context,
                           @StringRes int keyId,
                           boolean value) {
        String key = context.getString(keyId);
        getEditor(context).putBoolean(key, value).apply();
    }


    static void putString(@NonNull Context context,
                          @StringRes int keyId,
                          @Nullable String value) {
        String key = context.getString(keyId);
        getEditor(context).putString(key, value).apply();
    }

    static void putString(@NonNull Context context,
                          @Nullable String key,
                          @Nullable String value) {
        getEditor(context).putString(key, value).apply();
    }

    static void putInteger(@NonNull Context context,
                           @StringRes int keyId,
                           int value) {
        String key = context.getString(keyId);
        getEditor(context).putInt(key, value).apply();
    }

    static void putIsnteger(@NonNull Context context,
                            @StringRes int keyId,
                            int value) {
        String key = context.getString(keyId);
        getEditor(context).putInt(key, value).apply();
    }

    static void putLong(@NonNull Context context, @StringRes int keyId, long value) {
        String key = context.getString(keyId);
        getEditor(context).putLong(key, value).apply();
    }

    static long getLong(@NonNull Context context, @StringRes int keyId, long defaultValue) {
        String key = context.getString(keyId);
        return getPreferences(context).getLong(key, defaultValue);
    }


    @NonNull
    private static SharedPreferences getPreferences(@NonNull Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    private static SharedPreferences.Editor getEditor(@NonNull Context context) {
        return getPreferences(context).edit();
    }
}
