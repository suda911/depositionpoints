package com.desposition.tinkoff.tinkoffdepositionpoints;

import android.app.Application;

import com.desposition.tinkoff.tinkoffdepositionpoints.components.DaggerNetComponent;
import com.desposition.tinkoff.tinkoffdepositionpoints.components.NetComponent;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.MyObjectBox;
import com.desposition.tinkoff.tinkoffdepositionpoints.module.AppModule;
import com.desposition.tinkoff.tinkoffdepositionpoints.module.NetModule;

import io.objectbox.BoxStore;


public class MyApp extends Application {
    private static NetComponent mNetComponent;
    public BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();
        boxStore = MyObjectBox.builder().androidContext(this).build();
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(getString(R.string.server)))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }
}