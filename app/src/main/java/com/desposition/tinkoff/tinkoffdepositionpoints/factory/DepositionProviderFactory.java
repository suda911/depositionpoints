package com.desposition.tinkoff.tinkoffdepositionpoints.factory;

import android.app.Application;

import com.desposition.tinkoff.tinkoffdepositionpoints.ApiService;
import com.desposition.tinkoff.tinkoffdepositionpoints.repository.DepositionPartnersRepository;
import com.desposition.tinkoff.tinkoffdepositionpoints.viewmodel.DepositionPartnersViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DepositionProviderFactory extends ViewModelProvider.NewInstanceFactory {

    @NonNull
    private DepositionPartnersRepository repository;

    @NonNull
    private Application application;

    @NonNull
    private ApiService apiService;

    public DepositionProviderFactory(@NonNull Application application, @NonNull ApiService apiService, @NonNull DepositionPartnersRepository repository) {
        this.repository = repository;
        this.application = application;
        this.apiService = apiService;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.equals(DepositionPartnersViewModel.class)) {
            //noinspection unchecked
            return (T) new DepositionPartnersViewModel(application, apiService, repository);
        }
        return super.create(modelClass);
    }
}