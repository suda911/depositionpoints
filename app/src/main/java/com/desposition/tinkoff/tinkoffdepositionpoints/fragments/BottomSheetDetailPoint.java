package com.desposition.tinkoff.tinkoffdepositionpoints.fragments;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.desposition.tinkoff.tinkoffdepositionpoints.MyApp;
import com.desposition.tinkoff.tinkoffdepositionpoints.R;
import com.desposition.tinkoff.tinkoffdepositionpoints.activity.DetailsActivity;
import com.desposition.tinkoff.tinkoffdepositionpoints.databinding.FragmentBtmBinding;
import com.desposition.tinkoff.tinkoffdepositionpoints.helper.SettingHelper;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner_;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point_;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.File;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import io.objectbox.Box;

public class BottomSheetDetailPoint extends BottomSheetDialogFragment {
    private static final String PARTNER_NAME = "PARTNER_NAME";
    private static final String NAME = "NAME";
    private static final String EXTERNAL_ID = "EXTERNAL_ID";

    private Box<Partner> partnerBox;
    private Box<Point> pointBox;

    private Point point;
    private Partner partner;

    private FragmentBtmBinding btmBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        btmBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_btm, container, false);
        partnerBox = ((MyApp) Objects.requireNonNull(getActivity()).getApplication()).getBoxStore().boxFor(Partner.class);
        pointBox = ((MyApp) Objects.requireNonNull(getActivity()).getApplication()).getBoxStore().boxFor(Point.class);
        return btmBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null && getArguments().getString(PARTNER_NAME) != null && getArguments().getString(EXTERNAL_ID) != null && getArguments().getString(NAME) != null) {
            String id = getArguments().getString(PARTNER_NAME);
            String externalId = getArguments().getString(EXTERNAL_ID);
            String namePoint = getArguments().getString(NAME);
            partner = partnerBox.query().equal(Partner_.id, Objects.requireNonNull(id)).build().findFirst();
            if (partner != null) {
                String url = getString(R.string.url_picture) + SettingHelper.getDensity(Objects.requireNonNull(getContext())) + File.separator + partner.getPicture();
                Glide.with(this).load(url).apply(RequestOptions.circleCropTransform()).into(btmBinding.imgPoint);
            }
            btmBinding.tvName.setText(namePoint);
            point = pointBox.query().equal(Point_.externalId, Objects.requireNonNull(externalId)).build().findFirst();
            if (point != null) {
                btmBinding.tvAddress.setText(point.getFullAddress());
            }
            btmBinding.clt.setOnClickListener(listener);
        } else {
            dismiss();
        }
    }


    private View.OnClickListener listener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(DetailsActivity.PARTNER_NAME, partner.getId());
            intent.putExtra(DetailsActivity.POINT_ID, point.getExternalId());
            Pair<View, String> p1 = Pair.create(btmBinding.tvName, getString(R.string.transition_name_name));
            Pair<View, String> p2 = Pair.create(btmBinding.tvAddress, getString(R.string.transition_name_address));
            Pair<View, String> p3 = Pair.create(btmBinding.imgPoint, getString(R.string.transition_name_picture));
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1, p2, p3);
            startActivity(intent, options.toBundle());
        }
    };

    public static BottomSheetDetailPoint newInstance(@NonNull String id, @NonNull String name, @NonNull String externalId) {
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putString(PARTNER_NAME, id);
        args.putString(EXTERNAL_ID, externalId);
        BottomSheetDetailPoint fragment = new BottomSheetDetailPoint();
        fragment.setArguments(args);
        return fragment;
    }

}