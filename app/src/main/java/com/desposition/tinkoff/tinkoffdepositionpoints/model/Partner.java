package com.desposition.tinkoff.tinkoffdepositionpoints.model;

import android.os.Build;
import android.text.Html;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.relation.ToMany;

@Entity
public class Partner {

    @Id
    private long _id;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("hasLocations")
    @Expose
    private boolean hasLocations;
    @SerializedName("isMomentary")
    @Expose
    private boolean isMomentary;
    @SerializedName("depositionDuration")
    @Expose
    private String depositionDuration;
    @SerializedName("limitations")
    @Expose
    private String limitations;
    @SerializedName("pointType")
    @Expose
    private String pointType;
    @SerializedName("externalPartnerId")
    @Expose
    private String externalPartnerId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("moneyMin")
    @Expose
    private int moneyMin;
    @SerializedName("moneyMax")
    @Expose
    private int moneyMax;
    @SerializedName("hasPreferentialDeposition")
    @Expose
    private boolean hasPreferentialDeposition;
    @SerializedName("limits")
    @Expose
    @Transient
    private List<Limit> limits = null;

    private ToMany<Limit> limitToMany;
    @SerializedName("dailyLimits")
    @Expose
    @Transient
    private List<DailyLimit> dailyLimits = null;

    private ToMany<DailyLimit> dailyLimitToMany;


    public boolean isHasLocations() {
        return hasLocations;
    }

    public boolean isMomentary() {
        return isMomentary;
    }

    public boolean isHasPreferentialDeposition() {
        return hasPreferentialDeposition;
    }

    public ToMany<Limit> getLimitToMany() {
        return limitToMany;
    }

    public void setLimitToMany(ToMany<Limit> limitToMany) {
        this.limitToMany = limitToMany;
    }

    public ToMany<DailyLimit> getDailyLimitToMany() {
        return dailyLimitToMany;
    }

    public void setDailyLimitToMany(ToMany<DailyLimit> dailyLimitToMany) {
        this.dailyLimitToMany = dailyLimitToMany;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public boolean getMomentary() {
        return isMomentary;
    }

    public void setMomentary(boolean momentary) {
        isMomentary = momentary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getHasLocations() {
        return hasLocations;
    }

    public void setHasLocations(boolean hasLocations) {
        this.hasLocations = hasLocations;
    }

    public boolean getIsMomentary() {
        return isMomentary;
    }

    public void setIsMomentary(boolean isMomentary) {
        this.isMomentary = isMomentary;
    }

    public String getDepositionDuration() {
        String result = depositionDuration.replace("&nbps", "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(result, Html.FROM_HTML_MODE_COMPACT).toString();
        } else {
            return Html.fromHtml(result, Html.FROM_HTML_MODE_LEGACY).toString();
        }
    }

    public void setDepositionDuration(String depositionDuration) {
        this.depositionDuration = depositionDuration;
    }

    public String getLimitations() {
        String result = limitations.replace("&nbps", "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(result, Html.FROM_HTML_MODE_COMPACT).toString();
        } else {
            return Html.fromHtml(result, Html.FROM_HTML_MODE_LEGACY).toString();
        }
    }

    public void setLimitations(String limitations) {
        this.limitations = limitations;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getExternalPartnerId() {
        return externalPartnerId;
    }

    public void setExternalPartnerId(String externalPartnerId) {
        this.externalPartnerId = externalPartnerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMoneyMin() {
        return moneyMin;
    }

    public void setMoneyMin(int moneyMin) {
        this.moneyMin = moneyMin;
    }

    public int getMoneyMax() {
        return moneyMax;
    }

    public void setMoneyMax(int moneyMax) {
        this.moneyMax = moneyMax;
    }

    public boolean getHasPreferentialDeposition() {
        return hasPreferentialDeposition;
    }

    public void setHasPreferentialDeposition(boolean hasPreferentialDeposition) {
        this.hasPreferentialDeposition = hasPreferentialDeposition;
    }

    public List<Limit> getLimits() {
        return limits;
    }

    public void setLimits(List<Limit> limits) {
        this.limits = limits;
    }

    public List<DailyLimit> getDailyLimits() {
        return dailyLimits;
    }

    public void setDailyLimits(List<DailyLimit> dailyLimits) {
        this.dailyLimits = dailyLimits;
    }

    public Partner() {
    }

    public Partner(String id, String name, String picture, String url, boolean hasLocations, boolean isMomentary, String depositionDuration, String limitations, String pointType, String externalPartnerId, String description, int moneyMin, int moneyMax, boolean hasPreferentialDeposition) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.url = url;
        this.hasLocations = hasLocations;
        this.isMomentary = isMomentary;
        this.depositionDuration = depositionDuration;
        this.limitations = limitations;
        this.pointType = pointType;
        this.externalPartnerId = externalPartnerId;
        this.description = description;
        this.moneyMin = moneyMin;
        this.moneyMax = moneyMax;
        this.hasPreferentialDeposition = hasPreferentialDeposition;
    }


}
