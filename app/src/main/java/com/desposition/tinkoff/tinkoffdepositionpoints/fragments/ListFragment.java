package com.desposition.tinkoff.tinkoffdepositionpoints.fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.desposition.tinkoff.tinkoffdepositionpoints.ApiService;
import com.desposition.tinkoff.tinkoffdepositionpoints.MyApp;
import com.desposition.tinkoff.tinkoffdepositionpoints.R;
import com.desposition.tinkoff.tinkoffdepositionpoints.activity.DetailsActivity;
import com.desposition.tinkoff.tinkoffdepositionpoints.adapters.RVAdapter;
import com.desposition.tinkoff.tinkoffdepositionpoints.databinding.FragmentListBinding;
import com.desposition.tinkoff.tinkoffdepositionpoints.factory.DepositionProviderFactory;
import com.desposition.tinkoff.tinkoffdepositionpoints.helper.SettingHelper;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;
import com.desposition.tinkoff.tinkoffdepositionpoints.repository.DepositionPartnersRepository;
import com.desposition.tinkoff.tinkoffdepositionpoints.viewmodel.DepositionPartnersViewModel;

import java.io.File;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import io.objectbox.Box;

public class ListFragment extends Fragment {

    private RVAdapter<Presenter> adapter;

    private DepositionPartnersViewModel viewModel;

    private Box<Partner> partnerBox;
    private Box<Point> pointBox;

    @Inject
    ApiService apiService;
    private FragmentListBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MyApp) Objects.requireNonNull(getActivity()).getApplication()).getNetComponent().inject(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
        partnerBox = ((MyApp) getActivity().getApplication()).getBoxStore().boxFor(Partner.class);
        pointBox = ((MyApp) getActivity().getApplication()).getBoxStore().boxFor(Point.class);
        viewModel = getDepositionPartnersViewModel();
        adapter = new RVAdapter<>(new Presenter(), R.layout.cardview);
        viewModel.getThrowableMutableLiveData().observe(this, throwable -> Toast.makeText(getActivity(), R.string.sarcasm_error, Toast.LENGTH_SHORT).show());
        viewModel.getPartnersListMutableLiveData().observe(this, adapter::setMap);
        viewModel.getListMutableLiveData().observe(this, adapter::setList);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
        return binding.getRoot();
    }

    @NonNull
    private DepositionPartnersViewModel getDepositionPartnersViewModel() {
        ViewModelProvider.Factory factory = new DepositionProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), apiService, new DepositionPartnersRepository(partnerBox, pointBox, apiService));
        return ViewModelProviders.of(this, factory).get(DepositionPartnersViewModel.class);
    }

    public class Presenter {
        public void click(View view, String externalId, String partnerName) {
            Context context = view.getContext();
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra(DetailsActivity.PARTNER_NAME, partnerName);
            intent.putExtra(DetailsActivity.POINT_ID, externalId);
            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.tv_name), context.getString(R.string.transition_name_name));
            Pair<View, String> p2 = Pair.create(view.findViewById(R.id.tv_address), context.getString(R.string.transition_name_address));
            Pair<View, String> p3 = Pair.create(view.findViewById(R.id.img_point), context.getString(R.string.transition_name_picture));
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ListFragment.this.getActivity(), p1, p2, p3);
            startActivity(intent, options.toBundle());
        }
    }


    @BindingAdapter("android:setImg")
    public static void setImage(ImageView view, String picture) {
        Context context = view.getContext();
        String url = context.getString(R.string.url_picture) + SettingHelper.getDensity(context) + File.separator + picture;
        Glide.with(context).load(url).apply(RequestOptions.circleCropTransform()).into(view);
    }

    public static ListFragment newInstance() {
        Bundle args = new Bundle();
        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
