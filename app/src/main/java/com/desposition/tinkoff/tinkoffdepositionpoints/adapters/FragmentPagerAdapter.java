package com.desposition.tinkoff.tinkoffdepositionpoints.adapters;

import android.util.SparseArray;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class FragmentPagerAdapter extends androidx.fragment.app.FragmentPagerAdapter {
    private String tabTitles[] = new String[]{"Карта", "Список"};
    private SparseArray<Fragment> fragmentSparseArray = new SparseArray<>();

    public FragmentPagerAdapter(FragmentManager fm, Fragment fragment, Fragment fragment2) {
        super(fm);
        fragmentSparseArray.put(0, fragment);
        fragmentSparseArray.put(1, fragment2);
    }

    @Override
    public int getCount() {
        return fragmentSparseArray.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentSparseArray.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}