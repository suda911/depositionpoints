package com.desposition.tinkoff.tinkoffdepositionpoints.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.desposition.tinkoff.tinkoffdepositionpoints.ApiService;
import com.desposition.tinkoff.tinkoffdepositionpoints.MyApp;
import com.desposition.tinkoff.tinkoffdepositionpoints.R;
import com.desposition.tinkoff.tinkoffdepositionpoints.factory.DepositionProviderFactory;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;
import com.desposition.tinkoff.tinkoffdepositionpoints.repository.DepositionPartnersRepository;
import com.desposition.tinkoff.tinkoffdepositionpoints.viewmodel.DepositionPartnersViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import io.objectbox.Box;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener {
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private DepositionPartnersViewModel viewModel;
    private GoogleMap googleMap;
    private Box<Partner> partnerBox;
    private Box<Point> pointBox;

    @Inject
    ApiService apiService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ((MyApp) Objects.requireNonNull(getActivity()).getApplication()).getNetComponent().inject(this);
        partnerBox = ((MyApp) getActivity().getApplication()).getBoxStore().boxFor(Partner.class);
        pointBox = ((MyApp) getActivity().getApplication()).getBoxStore().boxFor(Point.class);
        SupportMapFragment mapFragment = (SupportMapFragment) Objects.requireNonNull(getChildFragmentManager()).findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);
        viewModel = getDepositionPartnersViewModel();
        viewModel.getThrowableMutableLiveData().observe(this, throwable -> Toast.makeText(getActivity(), throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show());
        viewModel.getPointsLiveData().observe(this, points ->
                {
                    for (Point point : points) {
                        if (googleMap != null)
                            googleMap.addMarker(new MarkerOptions()
                                    .position(point.getLatLng())
                                    .icon(bitmapDescriptorFromVector(getContext()))
                                    .title(point.getPartnerName())
                                    .snippet(point.getExternalId()));
                    }
                }
        );
        viewModel.checkPartners();
        viewModel.setDensity(Objects.requireNonNull(getActivity()));
        viewModel.getLocationMutableLiveData().observe(this, location -> viewModel.getPointsLiveData());
        viewModel.getThrowableMutableLiveData().observe(this, throwable -> Toast.makeText(getActivity(), R.string.throwable, Toast.LENGTH_SHORT).show());
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnCameraIdleListener(this);
        googleMap.setOnMarkerClickListener(this);
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(47.241936, 39.688222), 14));
        checkLocationPermission();

    }

    @NonNull
    private DepositionPartnersViewModel getDepositionPartnersViewModel() {
        ViewModelProvider.Factory factory = new DepositionProviderFactory(Objects.requireNonNull(getActivity()).getApplication(), apiService, new DepositionPartnersRepository(partnerBox, pointBox, apiService));
        return ViewModelProviders.of(this, factory).get(DepositionPartnersViewModel.class);
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (googleMap != null && !googleMap.isMyLocationEnabled())
                googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (googleMap != null && !googleMap.isMyLocationEnabled())
                googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (googleMap != null && !googleMap.isMyLocationEnabled())
                            googleMap.setMyLocationEnabled(true);
                    }
                }
                return;
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (viewModel != null) {
            if (googleMap != null) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()), new GoogleMap.CancelableCallback() {
                    public void onFinish() {
                        Partner partner = viewModel.getPartner(marker.getTitle());
                        if (partner != null) {
                            BottomSheetDetailPoint.newInstance(partner.getId(), partner.getName(), marker.getSnippet())
                                    .show(Objects.requireNonNull(getFragmentManager()), BottomSheetDetailPoint.class.getSimpleName());
                        }
                    }

                    public void onCancel() {
                    }
                });
            }
            return true;
        }
        return false;
    }

    public void onCameraIdle() {
        if (googleMap != null) {
            Projection projection = googleMap.getProjection();
            LatLng latLng = projection.getVisibleRegion().latLngBounds.getCenter();
            if (viewModel != null)
                viewModel.setLocation(latLng.latitude, latLng.longitude);
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, R.drawable.pin);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static MapFragment newInstance() {
        Bundle args = new Bundle();
        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
