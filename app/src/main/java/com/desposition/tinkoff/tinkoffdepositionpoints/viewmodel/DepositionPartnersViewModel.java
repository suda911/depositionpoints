package com.desposition.tinkoff.tinkoffdepositionpoints.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.desposition.tinkoff.tinkoffdepositionpoints.ApiService;
import com.desposition.tinkoff.tinkoffdepositionpoints.helper.SettingHelper;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Location;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Payload;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;
import com.desposition.tinkoff.tinkoffdepositionpoints.repository.DepositionPartnersRepository;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.objectbox.android.ObjectBoxLiveData;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class DepositionPartnersViewModel extends AndroidViewModel {

    private final double latitude = 47.241936; // default value
    private final double longitude = 39.688222; // default value


    @NonNull
    private DepositionPartnersRepository repository;

    @NonNull
    private Application application;

    @NonNull
    private ApiService apiService;

    public DepositionPartnersViewModel(@NonNull Application application, @NonNull ApiService apiService, @NonNull DepositionPartnersRepository repository) {
        super(application);
        this.repository = repository;
        this.application = application;
        this.apiService = apiService;
    }

    @Nullable
    private MutableLiveData<List<Point>> mutableLiveData;

    @Nullable
    private MutableLiveData<Location> locationMutableLiveData;

    @Nullable
    private MutableLiveData<Throwable> throwableMutableLiveData;

    @Nullable
    private ObjectBoxLiveData<Point> listMutableLiveData;

    @Nullable
    private ObjectBoxLiveData<Partner> partnerObjectBoxLiveData;

    @SuppressLint("CheckResult")
    @NonNull
    public ObjectBoxLiveData<Partner> getPartnersListMutableLiveData() {
        if (partnerObjectBoxLiveData == null) {
            partnerObjectBoxLiveData = new ObjectBoxLiveData<>(repository.getPartnersBox().query().build());
        }
        return partnerObjectBoxLiveData;
    }

    @NonNull
    public MutableLiveData<Location> getLocationMutableLiveData() {
        if (locationMutableLiveData == null) {
            locationMutableLiveData = new MutableLiveData<>();
            Location location = new Location(latitude, longitude);
            locationMutableLiveData.setValue(location);
        }
        return locationMutableLiveData;
    }

    public void setLocation(double latitude, double longitude) {
        if (locationMutableLiveData != null) {
            Location location = new Location(latitude, longitude);
            locationMutableLiveData.setValue(location);
        }
    }

    @SuppressLint("CheckResult")
    @NonNull
    public ObjectBoxLiveData<Point> getListMutableLiveData() {
        if (listMutableLiveData == null) {
            listMutableLiveData = new ObjectBoxLiveData<>(repository.getPointBox().query().build());
        }
        return listMutableLiveData;
    }

    @NonNull
    public MutableLiveData<Throwable> getThrowableMutableLiveData() {
        if (throwableMutableLiveData == null) {
            throwableMutableLiveData = new MutableLiveData<>();
        }
        return throwableMutableLiveData;
    }

    @SuppressLint("CheckResult")
    public LiveData<List<Point>> getPointsLiveData() {
        if (mutableLiveData == null) {
            mutableLiveData = new MutableLiveData<>();
        }
        if (System.currentTimeMillis() - (1000 * 60 * 10) > SettingHelper.getLastUpdate(application)) {
            repository.clearCash();
        }
        repository.getAllPayloadPoints(getLocationMutableLiveData().getValue().getLatitude(), getLocationMutableLiveData().getValue().getLongitude(), 1000)
                .subscribe((pointPayload) -> {
                            mutableLiveData.setValue(pointPayload.getPayload());
                            SettingHelper.setLastUpdate(application, System.currentTimeMillis());
                        },
                        throwable -> throwableMutableLiveData.setValue(throwable));
        return mutableLiveData;
    }

    @SuppressLint("CheckResult")
    public void checkPartners() {
        apiService.getPartnersHead()
                .subscribeOn(Schedulers.io())
                .filter(voidResponse -> {
                    String lastModify = voidResponse.raw().header("Last-Modified");
                    if (lastModify != null && !SettingHelper.getLastModifyPartners(application).equals(lastModify)) {
                        SettingHelper.setTmpLastModifyPartners(application, lastModify);
                        return true;
                    }
                    return false;
                })
                .toObservable()
                .flatMap((Function<Response<Void>, Observable<Payload<Partner>>>) voidResponse -> repository.getPartners())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(payloadObservable -> SettingHelper.setLastModifyPartners(application, SettingHelper.getTmpLastModifyPartners(application)),
                        throwable -> Objects.requireNonNull(throwableMutableLiveData).setValue(throwable));
    }

    public void setDensity(@NonNull Context context) {
        if (SettingHelper.getDensity(context) == null) {
            SettingHelper.setDensity(context, getDensityName(context));
        }
    }

    private static String getDensityName(@Nullable Context context) {
        if (context != null) {
            float density = context.getResources().getDisplayMetrics().density;
            if (density >= 3.0) {
                return "xxhdpi";
            }
            if (density >= 2.0) {
                return "xhdpi";
            }
            if (density >= 1.5) {
                return "hdpi";
            }
            if (density >= 1.0) {
                return "mdpi";
            }
            return "ldpi";
        }
        return "mdpi";
    }

    @Nullable
    public Partner getPartner(@Nullable String id) {
        if (id != null)
            return repository.getPartner(id);
        else
            return null;
    }
}
