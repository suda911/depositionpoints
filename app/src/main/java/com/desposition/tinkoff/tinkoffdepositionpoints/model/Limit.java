package com.desposition.tinkoff.tinkoffdepositionpoints.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.relation.ToOne;

@Entity
public class Limit {

    @Id
    long id;

    @SerializedName("currency")
    @Expose
    @Transient
    private Currency currency;

    private ToOne<Currency> currencyToOne;

    @SerializedName("min")
    @Expose
    private int min;
    @SerializedName("max")
    @Expose
    private int max;

    public Limit(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public Limit() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ToOne<Currency> getCurrencyToOne() {
        return currencyToOne;
    }

    public void setCurrencyToOne(ToOne<Currency> currencyToOne) {
        this.currencyToOne = currencyToOne;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

}