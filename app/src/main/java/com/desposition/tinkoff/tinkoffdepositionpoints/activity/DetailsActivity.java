package com.desposition.tinkoff.tinkoffdepositionpoints.activity;

import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.Window;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.desposition.tinkoff.tinkoffdepositionpoints.MyApp;
import com.desposition.tinkoff.tinkoffdepositionpoints.R;
import com.desposition.tinkoff.tinkoffdepositionpoints.databinding.ActivityDetailsBinding;
import com.desposition.tinkoff.tinkoffdepositionpoints.helper.SettingHelper;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner_;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point_;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import io.objectbox.Box;

public class DetailsActivity extends AppCompatActivity {

    public static final String PARTNER_NAME = "PARTNER_NAME";
    public static final String POINT_ID = "POINT_ID";
    ActivityDetailsBinding binding;

    Box<Partner> partnerBox;
    Box<Point> pointBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.shared_element_transation));
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);

        partnerBox = ((MyApp) getApplication()).getBoxStore().boxFor(Partner.class);
        pointBox = ((MyApp) getApplication()).getBoxStore().boxFor(Point.class);

        if (getIntent().getExtras() != null) {
            String partnerName = getIntent().getExtras().getString(PARTNER_NAME);
            String pointId = getIntent().getExtras().getString(POINT_ID);
            if (partnerName != null && pointId != null) {
                Partner partner = partnerBox.query().equal(Partner_.id, partnerName).build().findFirst();
                Point point = pointBox.query().equal(Point_.externalId, pointId).build().findFirst();
                if (partner != null && point != null) {
                    point.setShow(true);
                    binding.tvName.setTransitionName(getString(R.string.transition_name_name));
                    binding.tvAddress.setTransitionName(getString(R.string.transition_name_address));
                    binding.imgPoint.setTransitionName(getString(R.string.transition_name_picture));
                    binding.setPartner(partner);
                    binding.setPoint(point);
                    String url = getString(R.string.url_picture) + SettingHelper.getDensity(this) + File.separator + partner.getPicture();
                    Glide.with(this).load(url).apply(RequestOptions.circleCropTransform()).into(binding.imgPoint);
                    pointBox.put(point);
                }
            }
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
