package com.desposition.tinkoff.tinkoffdepositionpoints.components;


import com.desposition.tinkoff.tinkoffdepositionpoints.activity.MainActivity;
import com.desposition.tinkoff.tinkoffdepositionpoints.fragments.ListFragment;
import com.desposition.tinkoff.tinkoffdepositionpoints.fragments.MapFragment;
import com.desposition.tinkoff.tinkoffdepositionpoints.module.AppModule;
import com.desposition.tinkoff.tinkoffdepositionpoints.module.NetModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(MainActivity mainActivity);

    void inject(MapFragment mapFragment);

    void inject(ListFragment listFragment);
}