package com.desposition.tinkoff.tinkoffdepositionpoints.helper;

import android.content.Context;

import com.desposition.tinkoff.tinkoffdepositionpoints.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SettingHelper {
    public static void setLastUpdate(@NonNull Context context, long date) {
        SettingUtil.putLong(context, R.string.primitive_last_update, date);
    }

    public static long getLastUpdate(@NonNull Context context) {
        return SettingUtil.getLong(context, R.string.primitive_last_update, System.currentTimeMillis());
    }

    public static void setLastModifyPartners(@NonNull Context context, @NonNull String date) {
        SettingUtil.putString(context, R.string.primitive_last_modify_partner, date);
    }

    public static String getLastModifyPartners(@NonNull Context context) {
        return SettingUtil.getString(context, R.string.primitive_last_modify_partner, "startValue");
    }

    public static void setTmpLastModifyPartners(@NonNull Context context, @NonNull String date) {
        SettingUtil.putString(context, R.string.primitive_last_modify_partner_tmp, date);
    }

    public static String getTmpLastModifyPartners(@NonNull Context context) {
        return SettingUtil.getString(context, R.string.primitive_last_modify_partner_tmp, "startValue");
    }

    public static void setDensity(@NonNull Context context, @NonNull String name) {
        SettingUtil.putString(context, R.string.primitive_density_name, name);
    }

    @Nullable
    public static String getDensity(@NonNull Context context) {
        return SettingUtil.getString(context, R.string.primitive_density_name, null);
    }

}
