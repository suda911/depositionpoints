package com.desposition.tinkoff.tinkoffdepositionpoints.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.relation.ToOne;

@Entity
public class DailyLimit {

    @Id
    long id;

    @SerializedName("currency")
    @Expose
    @Transient
    private Currency currency;

    private ToOne<Currency> currencyToOne;

    @SerializedName("amount")
    @Expose
    private int amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ToOne<Currency> getCurrencyToOne() {
        return currencyToOne;
    }

    public void setCurrencyToOne(ToOne<Currency> currencyToOne) {
        this.currencyToOne = currencyToOne;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public DailyLimit(int amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public DailyLimit() {
    }
}