package com.desposition.tinkoff.tinkoffdepositionpoints.repository;

import com.desposition.tinkoff.tinkoffdepositionpoints.ApiService;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Currency;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.DailyLimit;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Limit;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Location;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner_;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Payload;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point_;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.objectbox.Box;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DepositionPartnersRepository {

    @NonNull
    private ApiService apiService;

    @NonNull
    private Box<Partner> partnerBox;

    @NonNull
    private Box<Point> pointBox;

    public DepositionPartnersRepository(@NonNull Box<Partner> partnerBox, @NonNull Box<Point> pointBox, @NonNull ApiService apiService) {
        this.apiService = apiService;
        this.partnerBox = partnerBox;
        this.pointBox = pointBox;
    }

    @NonNull
    public Observable<Payload<Point>> getAllPayloadPoints(double latitude, double longitude, int radius) {
        return apiService.getAllPayload(latitude, longitude, radius)
                .subscribeOn(Schedulers.io())
                .doOnNext(pointPayload -> {
                    for (Point item : pointPayload.getPayload()) {
                        if (pointBox.query().equal(Point_.externalId, item.getExternalId()).build().findFirst() == null) {
                            Point point = item.getPoint();
                            Location location = new Location(item.getLocation().getLatitude(), item.getLocation().getLongitude());
                            point.getLocationToOne().setTarget(location);
                            pointBox.put(point);
                        }
                    }
                })
                .onErrorReturn(throwable -> new Payload<>(pointBox.getAll()))
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void clearCash() {
        pointBox.removeAll();
    }

    public Observable<Payload<Partner>> getPartners() {
        return apiService.getPartners()
                .subscribeOn(Schedulers.io())
                .doOnNext(partnerPayload -> {
                    for (Partner partner : partnerPayload.getPayload()) {
                        if (partner.getDailyLimits() != null && partner.getDailyLimits().size() > 0) {
                            for (DailyLimit dailyLimitItem : partner.getDailyLimits()) {
                                Currency currencyItem = dailyLimitItem.getCurrency();
                                Currency currency = new Currency(currencyItem.getCode(), currencyItem.getName(), currencyItem.getStrCode());
                                DailyLimit dailyLimit = new DailyLimit(dailyLimitItem.getAmount());
                                dailyLimit.getCurrencyToOne().setTarget(currency);
                                partner.getDailyLimitToMany().add(dailyLimit);
                            }
                        }
                        if (partner.getLimits() != null && partner.getLimits().size() > 0) {
                            for (Limit limitItem : partner.getLimits()) {
                                Currency currencyItem = limitItem.getCurrency();
                                Currency currency = new Currency(currencyItem.getCode(), currencyItem.getName(), currencyItem.getStrCode());
                                Limit limit = new Limit(limitItem.getMin(), limitItem.getMax());
                                limit.getCurrencyToOne().setTarget(currency);
                                partner.getLimitToMany().add(limit);
                            }
                        }
                    }
                })
                .doOnNext(partnerPayload -> partnerBox.put(partnerPayload.getPayload()));
    }

    @NonNull
    public Box<Partner> getPartnersBox() {
        return partnerBox;
    }

    @NonNull
    public Box<Point> getPointBox() {
        return pointBox;
    }

    @Nullable
    public Partner getPartner(@NonNull String id) {
        return partnerBox.query().equal(Partner_.id, id).build().findFirst();
    }
}
