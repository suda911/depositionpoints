package com.desposition.tinkoff.tinkoffdepositionpoints.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Payload<P> {
    @SerializedName("resultCode")
    @Expose
    private String resultCode;
    @SerializedName("payload")
    @Expose
    private List<P> payload = null;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public List<P> getPayload() {
        return payload;
    }

    public void setPayload(List<P> payload) {
        this.payload = payload;
    }

    public Payload() {
    }

    public Payload(List<P> payload) {
        this.payload = payload;
    }
}
