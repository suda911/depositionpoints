package com.desposition.tinkoff.tinkoffdepositionpoints.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

public class RVAdapter<P> extends RecyclerView.Adapter<RVAdapter.ViewHolder> {

    private List<Point> list;
    private P presenter;
    private int holderLayout;
    public Map<String, Partner> map;


    public RVAdapter(@NonNull P presenter, int holderLayout) {
        this.list = new ArrayList<>();
        this.presenter = presenter;
        this.holderLayout = holderLayout;
    }

    @NonNull
    @Override
    public RVAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(holderLayout, parent, false);
        return new RVAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapter.ViewHolder holder, int position) {
        holder.viewDataBinding.setVariable(BR.item, list.get(holder.getAdapterPosition()));
        holder.viewDataBinding.setVariable(BR.partner, map.get(list.get(holder.getAdapterPosition()).getPartnerName()));
        holder.viewDataBinding.setVariable(BR.position, holder.getAdapterPosition());
        holder.viewDataBinding.setVariable(BR.presenter, presenter);
    }

    public void setList(@NonNull List<Point> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setMap(@NonNull List<Partner> list) {
        map = new HashMap<>();
        for (Partner partner : list) {
            map.put(partner.getId(), partner);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding viewDataBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            viewDataBinding = DataBindingUtil.bind(itemView);
        }
    }
}
