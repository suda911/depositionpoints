package com.desposition.tinkoff.tinkoffdepositionpoints.livedata;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.lifecycle.LiveData;

import static android.content.Context.LOCATION_SERVICE;

@Deprecated
public class LocationLiveData extends LiveData<Location> implements LocationListener {
    private final static int UPDATE_INTERVAL = 1000;

    private LocationManager manager;
    private Context context;

    private boolean isGPSEnabled = false;

    private boolean isNetworkEnabled = false;

    public LocationLiveData(Context context) {
        manager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        this.context = context;
        if (manager != null) {
            isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
    }

    @Override
    protected void onActive() {
        if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isGPSEnabled) {
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL, 2, this);
        } else if (isNetworkEnabled) {
            manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, UPDATE_INTERVAL, 2, this);
        }
    }

    @Override
    protected void onInactive() {
        if (manager != null)
            manager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        setValue(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}