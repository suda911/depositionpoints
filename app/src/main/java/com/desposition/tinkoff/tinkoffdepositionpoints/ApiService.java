package com.desposition.tinkoff.tinkoffdepositionpoints;


import com.desposition.tinkoff.tinkoffdepositionpoints.model.Partner;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Payload;
import com.desposition.tinkoff.tinkoffdepositionpoints.model.Point;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.Query;

public interface ApiService {

    @GET("deposition_points")
    Observable<Payload<Point>> getAllPayload(
            @Query("latitude") double latitude,
            @Query("longitude") double longitude,
            @Query("radius") int radius
    );

    @GET("deposition_partners?accountType=Credit")
    Observable<Payload<Partner>> getPartners();

    @HEAD("deposition_partners?accountType=Credit")
    Single<Response<Void>> getPartnersHead();
}
