package com.desposition.tinkoff.tinkoffdepositionpoints.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Currency {

    @Id
    long id;

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("strCode")
    @Expose
    private String strCode;

    public Currency(int code, String name, String strCode) {
        this.code = code;
        this.name = name;
        this.strCode = strCode;
    }

    public Currency() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStrCode() {
        return strCode;
    }

    public void setStrCode(String strCode) {
        this.strCode = strCode;
    }

}