package com.desposition.tinkoff.tinkoffdepositionpoints.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.desposition.tinkoff.tinkoffdepositionpoints.R;
import com.desposition.tinkoff.tinkoffdepositionpoints.adapters.FragmentPagerAdapter;
import com.desposition.tinkoff.tinkoffdepositionpoints.databinding.ActivityMainBinding;
import com.desposition.tinkoff.tinkoffdepositionpoints.fragments.ListFragment;
import com.desposition.tinkoff.tinkoffdepositionpoints.fragments.MapFragment;

import java.util.List;
import java.util.Objects;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

public class MainActivity extends FragmentActivity {
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager(), MapFragment.newInstance(), ListFragment.newInstance());
        binding.viewPager.setAdapter(adapter);
        binding.tab.setupWithViewPager(binding.viewPager);
        checkLocationPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        List<Fragment> fragments = getSupportFragmentManager().getFragments();
                        for (Fragment fragment : fragments) {
                            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                        }
                    }
                }
                return;
            }
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(this), Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(Objects.requireNonNull(this),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLocationPermission();
    }
}
