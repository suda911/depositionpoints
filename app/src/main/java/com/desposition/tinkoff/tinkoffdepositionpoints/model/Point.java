package com.desposition.tinkoff.tinkoffdepositionpoints.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.relation.ToOne;

@Entity
public class Point {

    @Id
    long id;

    @SerializedName("externalId")
    @Expose
    private String externalId;
    @SerializedName("partnerName")
    @Expose
    private String partnerName;
    @SerializedName("location")
    @Expose
    @Transient
    private Location location;

    private ToOne<Location> locationToOne;

    @SerializedName("workHours")
    @Expose
    private String workHours;
    @SerializedName("phones")
    @Expose
    private String phones;
    @SerializedName("addressInfo")
    @Expose
    private String addressInfo;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("bankInfo")
    @Expose
    private String bankInfo;

    private boolean show = false;

    public Point() {
    }

    public Point(String externalId, String partnerName, String workHours, String phones, String addressInfo, String fullAddress, String bankInfo) {
        this.externalId = externalId;
        this.partnerName = partnerName;
        this.workHours = workHours;
        this.phones = phones;
        this.addressInfo = addressInfo;
        this.fullAddress = fullAddress;
        this.bankInfo = bankInfo;
        this.show = false;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    @NonNull
    public LatLng getLatLng() {
        try{
            return new LatLng(location.getLatitude(), location.getLongitude());
        }catch (Exception e){
            return new LatLng(locationToOne.getTarget().getLatitude(), locationToOne.getTarget().getLongitude());
        }
    }

    public void setLocation(long locationId) {
        this.locationToOne.setTargetId(locationId);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ToOne<Location> getLocationToOne() {
        return locationToOne;
    }

    public void setLocationToOne(ToOne<Location> locationToOne) {
        this.locationToOne = locationToOne;
    }

    public String getWorkHours() {
        return workHours;
    }

    public void setWorkHours(String workHours) {
        this.workHours = workHours;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public String getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(String addressInfo) {
        this.addressInfo = addressInfo;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(String bankInfo) {
        this.bankInfo = bankInfo;
    }

    @NonNull
    public Point getPoint() {
        return new Point(externalId, partnerName, workHours, phones, addressInfo, fullAddress, bankInfo);
    }
}
